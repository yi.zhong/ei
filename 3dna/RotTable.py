from json import load as json_load
from os import path as os_path
from .Traj3D import Traj3D
import random as rd
import numpy as np
import json
from .Constantes import *
import math
here = os_path.abspath(os_path.dirname(__file__))


class RotTable:
    """Represents a rotation table"""

    # 3 first values: 3 angle values
    # 3 last values: SD values
    filename = os_path.join(here, 'table.json')
    base_rot_table = json_load(open(filename))

    def __init__(self, filename: str = None, randomize=False):
        self.dist_carre = None
        if filename is None:
            filename = os_path.join(here, 'table.json')
        self.rot_table = json_load(open(filename))
        if randomize:
            for dinuc in List_of_genes:
                baseTwist = RotTable.base_rot_table[dinuc][0]
                baseWedge = RotTable.base_rot_table[dinuc][1]
                deltaTwist = RotTable.base_rot_table[dinuc][3]
                deltaWedge = RotTable.base_rot_table[dinuc][4]
                newTwist = rd.uniform(
                    baseTwist-deltaTwist, baseTwist+deltaTwist)
                newWedge = rd.uniform(
                    baseWedge-deltaWedge, baseWedge+deltaWedge)
                # si ne fonctionne pas, il faut faire a la main
                self.setTwist(dinuc, newTwist)
                self.setWedge(dinuc, newWedge)

    ###################
    # WRITING METHODS #
    ###################
    def setTwist(self, dinucleotide: str, value: float):
        self.rot_table[dinucleotide][0] = value
        if Symetrique[dinucleotide] is not None:
            self.rot_table[Symetrique[dinucleotide]][0] = value

    def setWedge(self, dinucleotide: str, value: float):
        self.rot_table[dinucleotide][1] = value
        if Symetrique[dinucleotide] is not None:
            self.rot_table[Symetrique[dinucleotide]][1] = value

    def setDirection(self, dinucleotide: str, value: float):
        self.rot_table[dinucleotide][2] = value
        if Symetrique[dinucleotide] is not None:
            self.rot_table[Symetrique[dinucleotide]][2] = - value

    ###################
    # READING METHODS #
    ###################
    def getTwist(self, dinucleotide: str):
        return self.getTable()[dinucleotide][0]

    def getWedge(self, dinucleotide: str):
        return self.getTable()[dinucleotide][1]

    def getDirection(self, dinucleotide: str):
        return self.getTable()[dinucleotide][2]

    def getTable(self):
        return self.rot_table

    def distance(self, molecule):  # renvoie la distance au carrée séparant les deux extrémités de la molecule telle que générée par la table self
        traj = Traj3D(False)
        traj.compute(molecule+molecule[0], self)

        n = len(traj.getTraj())
        x0 = traj.getTraj()[0][0]
        y0 = traj.getTraj()[0][1]
        z0 = traj.getTraj()[0][2]
        xf = traj.getTraj()[n-1][0]
        yf = traj.getTraj()[n-1][1]
        zf = traj.getTraj()[n-1][2]
        self.dist_carre = (xf-x0)**2 + (yf-y0)**2 + (zf-z0)**2
        return math.sqrt(self.dist_carre)

    def angle(self, molecule):
        traj = Traj3D(False)
        traj.compute(molecule+molecule[:AGRANDISSEMENT], self)
        X = traj.getTraj()[0]
        Y = traj.getTraj()[1]
        Z = traj.getTraj()[-2]
        T = traj.getTraj()[-1]

        def prod_scalaire(X, Y, Z, T):
            s = 0
            for i in range(3):
                s += (Y[i]-X[i])*(T[i]-Z[i])
            return s

        def norme(X, Y):
            s = 0
            for i in range(3):
                s += (Y[i]-X[i])**2
            return s
        return math.acos(prod_scalaire(X, Y, Z, T)/(norme(X, Y)*norme(Z, T)))

    def evaluate(self, molecule):
        """prend en compte produit scalaire et distance"""
        def prod_scalaire(X, Y, Z, T):
            s = 0
            for i in range(3):
                s += (Y[i]-X[i])*(T[i]-Z[i])
            return s
        traj = Traj3D(False)
        traj.compute(molecule+molecule[:AGRANDISSEMENT], self)

        res = 0
        n = len(traj.getTraj())
        for i in range(AGRANDISSEMENT):
            x0 = traj.getTraj()[i][0]
            y0 = traj.getTraj()[i][1]
            z0 = traj.getTraj()[i][2]
            xf = traj.getTraj()[n-AGRANDISSEMENT+i][0]
            yf = traj.getTraj()[n-AGRANDISSEMENT+i][1]
            zf = traj.getTraj()[n-AGRANDISSEMENT+i][2]
            res = res + (xf-x0)**2 + (yf-y0)**2 + (zf-z0)**2
        X = traj.getTraj()[0]
        Y = traj.getTraj()[1]
        Z = traj.getTraj()[-2]
        T = traj.getTraj()[-1]
        return (res/AGRANDISSEMENT - prod_scalaire(X, Y, Z, T)*Prise_en_compte_angle)

    def evaluate_en_plusieurs_points(self, molecule):
        n = len(molecule)
        summ = self.evaluate(molecule)
        for k in range(ALPHA):
            i = rd.randint(1, n)
            newmol = molecule[i:] + molecule[:i]
            summ = summ + self.evaluate(newmol)
        return summ/ALPHA

    def evaluate_partout(self, molecule):
        traj = Traj3D(False)
        double_molecule = molecule+molecule
        traj.compute(double_molecule, self)
        res = 0
        n = len(traj.getTraj())//2
        for i in range(0, n, pas):
            x0 = traj.getTraj()[i][0]
            y0 = traj.getTraj()[i][1]
            z0 = traj.getTraj()[i][2]
            xf = traj.getTraj()[n+i][0]
            yf = traj.getTraj()[n+i][1]
            zf = traj.getTraj()[n+i][2]
            res = res + ((xf-x0)**2 + (yf-y0)**2 + (zf-z0)**2)**(1/2)
        return res/(n//pas)

    def copy(self):
        copy = RotTable()
        for nuc1 in ('A', 'G', 'C', 'T'):
            for nuc2 in ('A', 'G', 'C', 'T'):
                dinuc = nuc1 + nuc2
                for i in range(6):
                    copy.rot_table[dinuc][i] = self.rot_table[dinuc][i]
        return copy

    def mutate(self, molecule):
        '''
        Entrée :
            - self : une RotTable
        Sortie :
            - nouv_personne : une RotTable avec un gene modifié aléatoirement uniformément sur l'espace des valeurs admissibles
        '''
        Pm = 0.3
        selected_gene = List_of_genes[rd.randint(0, len(List_of_genes)-1)]
        nb_feature = 3  # 2 suffiraient dans notre cas mais on généralise
        selected_feature = np.random.binomial(1, Pm, nb_feature)
        if selected_feature[0] == 1:
            baseTwist = RotTable.base_rot_table[selected_gene][0]
            deltaTwist = RotTable.base_rot_table[selected_gene][3]
            newTwist = rd.uniform(baseTwist-deltaTwist, baseTwist+deltaTwist)
            self.setTwist(selected_gene, newTwist)
        if selected_feature[1] == 1:
            baseWedge = RotTable.base_rot_table[selected_gene][1]
            deltaWedge = RotTable.base_rot_table[selected_gene][4]
            newWedge = rd.uniform(baseWedge-deltaWedge, baseWedge+deltaWedge)
            self.setWedge(selected_gene, newWedge)
        if selected_feature[2] == 1:
            baseDirection = RotTable.base_rot_table[selected_gene][2]
            deltaDirection = RotTable.base_rot_table[selected_gene][5]
            newDirection = rd.uniform(
                baseDirection-deltaDirection, baseDirection+deltaDirection)
            self.setDirection(selected_gene, newDirection)

    def mutate_proximite(self, molecule):
        Pm = 0.2  # à choisir dans [0,001 ; 0,01]
        nb_feature = 3  # 2 suffiraient dans notre cas mais on généralise
        parametre_preced = self.getTable()
        for selected_gene in List_of_genes:
            if rd.random() < Pm:
                baseTwist = RotTable.base_rot_table[selected_gene][0]
                deltaTwist = RotTable.base_rot_table[selected_gene][3]
                newTwist = rd.uniform(max(baseTwist-deltaTwist, parametre_preced[selected_gene][0]-deltaTwist/5),
                                      min(baseTwist+deltaTwist, parametre_preced[selected_gene][0]+deltaTwist/5))
                self.setTwist(selected_gene, newTwist)

                baseWedge = RotTable.base_rot_table[selected_gene][1]
                deltaWedge = RotTable.base_rot_table[selected_gene][4]
                newWedge = rd.uniform(max(baseWedge-deltaWedge, parametre_preced[selected_gene][1]-deltaWedge/5),
                                      min(baseWedge+deltaWedge, parametre_preced[selected_gene][1]+deltaWedge/5))
                self.setWedge(selected_gene, newWedge)

                baseDirection = RotTable.base_rot_table[selected_gene][2]
                deltaDirection = RotTable.base_rot_table[selected_gene][5]
                newDirection = rd.uniform(max(baseDirection-deltaDirection, parametre_preced[selected_gene][2]-deltaDirection/5), min(
                    baseDirection+deltaDirection, parametre_preced[selected_gene][2]+deltaDirection/5))
                self.setDirection(selected_gene, newDirection)

    def mutate_degressif(self, molecule):
        Pm = 0.2  # à choisir dans [0,001 ; 0,01]
        score = max(self.evaluate(molecule)/(6000*AGRANDISSEMENT), 1)

        parametre_preced = self.getTable()
        for selected_gene in List_of_genes:
            if rd.random() < Pm:
                baseTwist = RotTable.base_rot_table[selected_gene][0]
                deltaTwist = RotTable.base_rot_table[selected_gene][3]
                newTwist = rd.uniform(max(baseTwist-deltaTwist, parametre_preced[selected_gene][0]-score*deltaTwist),
                                      min(baseTwist+deltaTwist, parametre_preced[selected_gene][0]+score*deltaTwist))
                self.setTwist(selected_gene, newTwist)

                baseWedge = RotTable.base_rot_table[selected_gene][1]
                deltaWedge = RotTable.base_rot_table[selected_gene][4]
                newWedge = rd.uniform(max(baseWedge-deltaWedge, parametre_preced[selected_gene][1]-score*deltaWedge),
                                      min(baseWedge+deltaWedge, parametre_preced[selected_gene][1]+score*deltaWedge))
                self.setWedge(selected_gene, newWedge)

                baseDirection = RotTable.base_rot_table[selected_gene][2]
                deltaDirection = RotTable.base_rot_table[selected_gene][5]
                newDirection = rd.uniform(max(baseDirection-deltaDirection, parametre_preced[selected_gene][2]-score*deltaDirection), min(
                    baseDirection+deltaDirection, parametre_preced[selected_gene][2]+score*deltaDirection))
                self.setDirection(selected_gene, newDirection)

    def exportation(self):
        with open("meilleur_table.json", "w") as outfile:
            json.dump(self.getTable(), outfile)

    def is_good_table(self):
        for dinuc in List_of_genes:
            if self.getTwist(dinuc) > RotTable.base_rot_table[dinuc][0] + RotTable.base_rot_table[dinuc][3]:
                return False
            if self.getTwist(dinuc) < RotTable.base_rot_table[dinuc][0] - RotTable.base_rot_table[dinuc][3]:
                return False
            if self.getWedge(dinuc) > RotTable.base_rot_table[dinuc][1] + RotTable.base_rot_table[dinuc][4]:
                return False
            if self.getWedge(dinuc) < RotTable.base_rot_table[dinuc][1] - RotTable.base_rot_table[dinuc][4]:
                return False
            if self.getDirection(dinuc) > RotTable.base_rot_table[dinuc][2] + RotTable.base_rot_table[dinuc][5]:
                return False
            if self.getDirection(dinuc) < RotTable.base_rot_table[dinuc][2] - RotTable.base_rot_table[dinuc][5]:
                return False
            if Symetrique[dinuc] is not None:
                if self.getTwist(dinuc) != self.getTwist(Symetrique[dinuc]):
                    return False
                if self.getWedge(dinuc) != self.getWedge(Symetrique[dinuc]):
                    return False
                if self.getDirection(dinuc) != self.getDirection(Symetrique[dinuc]):
                    return False
        return True

    ###################

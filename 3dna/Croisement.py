from .RotTable import RotTable
import random as rd
from .Constantes import *


def croisement_en_un_point(personne1, personne2):
    '''
    Entrées :
        - personne1, personne2 : deux fichiers de classe RotTable
    Sortie
        - liste avec personne3, personne4 : liste de RotTable
    '''
    indice_coupe = rd.randint(0, len(List_of_genes)-2)  # coupe au milieu
    personne3 = RotTable()
    personne4 = RotTable()
    for i in range(len(List_of_genes)):
        if i <= indice_coupe:
            personne3.setTwist(
                List_of_genes[i], personne1.getTwist(List_of_genes[i]))
            personne3.setWedge(
                List_of_genes[i], personne1.getWedge(List_of_genes[i]))
            personne3.setDirection(
                List_of_genes[i], personne1.getDirection(List_of_genes[i]))
            personne4.setTwist(
                List_of_genes[i], personne2.getTwist(List_of_genes[i]))
            personne4.setWedge(
                List_of_genes[i], personne2.getWedge(List_of_genes[i]))
            personne4.setDirection(
                List_of_genes[i], personne2.getDirection(List_of_genes[i]))
        else:
            personne3.setTwist(
                List_of_genes[i], personne2.getTwist(List_of_genes[i]))
            personne3.setWedge(
                List_of_genes[i], personne2.getWedge(List_of_genes[i]))
            personne3.setDirection(
                List_of_genes[i], personne2.getDirection(List_of_genes[i]))
            personne4.setTwist(
                List_of_genes[i], personne1.getTwist(List_of_genes[i]))
            personne4.setWedge(
                List_of_genes[i], personne1.getWedge(List_of_genes[i]))
            personne4.setDirection(
                List_of_genes[i], personne1.getDirection(List_of_genes[i]))
    return [personne3, personne4]


def croisement_en_deux_points(personne1, personne2):
    '''
    Entrées :
        - personne1, personne2 : deux fichiers de classe RotTable
    Sortie
        - liste avec personne3, personne4 : une RotTable
    '''
    indice_coupe1 = rd.randint(1, len(List_of_genes)-2)
    indice_coupe2 = rd.randint(indice_coupe1, len(List_of_genes)-1)
    personne3 = RotTable()
    personne4 = RotTable()

    for i in range(len(List_of_genes)):
        if i <= indice_coupe1:
            personne3.setTwist(
                List_of_genes[i], personne1.getTwist(List_of_genes[i]))
            personne3.setWedge(
                List_of_genes[i], personne1.getWedge(List_of_genes[i]))
            personne3.setDirection(
                List_of_genes[i], personne1.getDirection(List_of_genes[i]))
            personne4.setTwist(
                List_of_genes[i], personne2.getTwist(List_of_genes[i]))
            personne4.setWedge(
                List_of_genes[i], personne2.getWedge(List_of_genes[i]))
            personne4.setDirection(
                List_of_genes[i], personne2.getDirection(List_of_genes[i]))
        elif i > indice_coupe1 and i <= indice_coupe2:
            personne3.setTwist(
                List_of_genes[i], personne2.getTwist(List_of_genes[i]))
            personne3.setWedge(
                List_of_genes[i], personne2.getWedge(List_of_genes[i]))
            personne3.setDirection(
                List_of_genes[i], personne2.getDirection(List_of_genes[i]))
            personne4.setTwist(
                List_of_genes[i], personne1.getTwist(List_of_genes[i]))
            personne4.setWedge(
                List_of_genes[i], personne1.getWedge(List_of_genes[i]))
            personne4.setDirection(
                List_of_genes[i], personne1.getDirection(List_of_genes[i]))
        else:
            personne3.setTwist(
                List_of_genes[i], personne2.getTwist(List_of_genes[i]))
            personne3.setWedge(
                List_of_genes[i], personne2.getWedge(List_of_genes[i]))
            personne3.setDirection(
                List_of_genes[i], personne2.getDirection(List_of_genes[i]))
            personne4.setTwist(
                List_of_genes[i], personne1.getTwist(List_of_genes[i]))
            personne4.setWedge(
                List_of_genes[i], personne1.getWedge(List_of_genes[i]))
            personne4.setDirection(
                List_of_genes[i], personne1.getDirection(List_of_genes[i]))
    return [personne3, personne4]


def croisement_milieu(personne1, personne2):
    '''
    Entrées :
        - personne1, personne2 : deux fichiers de classe RotTable
    Sortie
        - liste avec personne3, personne4 : liste de RotTable
    '''
    i_coupe = rd.randint(0, len(List_of_genes)-2)  # coupe au milieu
    nb_feature = 3
    j_inside_coupe = rd.randint(0, nb_feature-1)
    personne3 = RotTable()
    personne4 = RotTable()
    for i in range(len(List_of_genes)):
        if i < i_coupe:
            personne3.setTwist(
                List_of_genes[i], personne1.getTwist(List_of_genes[i]))
            personne3.setWedge(
                List_of_genes[i], personne1.getWedge(List_of_genes[i]))
            personne3.setDirection(
                List_of_genes[i], personne1.getDirection(List_of_genes[i]))
            personne4.setTwist(
                List_of_genes[i], personne2.getTwist(List_of_genes[i]))
            personne4.setWedge(
                List_of_genes[i], personne2.getWedge(List_of_genes[i]))
            personne4.setDirection(
                List_of_genes[i], personne2.getDirection(List_of_genes[i]))
        elif i == i_coupe:
            if j_inside_coupe >= 1:
                personne3.setTwist(
                    List_of_genes[i], personne1.getTwist(List_of_genes[i]))
                personne4.setTwist(
                    List_of_genes[i], personne2.getTwist(List_of_genes[i]))
            if j_inside_coupe >= 2:
                personne3.setWedge(
                    List_of_genes[i], personne1.getWedge(List_of_genes[i]))
                personne4.setWedge(
                    List_of_genes[i], personne2.getWedge(List_of_genes[i]))
            if j_inside_coupe >= 3:
                personne3.setDirection(
                    List_of_genes[i], personne1.getDirection(List_of_genes[i]))
                personne4.setDirection(
                    List_of_genes[i], personne2.getDirection(List_of_genes[i]))
            if j_inside_coupe < 1:
                personne3.setTwist(
                    List_of_genes[i], personne2.getTwist(List_of_genes[i]))
                personne4.setTwist(
                    List_of_genes[i], personne1.getTwist(List_of_genes[i]))
            if j_inside_coupe < 2:
                personne3.setWedge(
                    List_of_genes[i], personne2.getWedge(List_of_genes[i]))
                personne4.setWedge(
                    List_of_genes[i], personne1.getWedge(List_of_genes[i]))
            if j_inside_coupe < 3:
                personne3.setDirection(
                    List_of_genes[i], personne2.getDirection(List_of_genes[i]))
                personne4.setDirection(
                    List_of_genes[i], personne1.getDirection(List_of_genes[i]))
        else:
            personne3.setTwist(
                List_of_genes[i], personne2.getTwist(List_of_genes[i]))
            personne3.setWedge(
                List_of_genes[i], personne2.getWedge(List_of_genes[i]))
            personne3.setDirection(
                List_of_genes[i], personne2.getDirection(List_of_genes[i]))
            personne4.setTwist(
                List_of_genes[i], personne1.getTwist(List_of_genes[i]))
            personne4.setWedge(
                List_of_genes[i], personne1.getWedge(List_of_genes[i]))
            personne4.setDirection(
                List_of_genes[i], personne1.getDirection(List_of_genes[i]))
    return [personne3, personne4]


Dico_Croisements = {"croisement_en_deux_points": croisement_en_deux_points,
                    "croisement_en_un_point": croisement_en_un_point,
                    "croisement_milieu": croisement_milieu}
